import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_transisitm/app/modules/home/views/add_view.dart';
import 'package:flutter_app_transisitm/app/modules/home/views/details_view.dart';
import 'package:flutter_app_transisitm/app/modules/login/controllers/login_controller.dart';
import 'package:flutter_app_transisitm/app/modules/login/providers/login_provider.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  HomeView({Key? key}) : super(key: key);

  final LoginController _loginController = Get.put(LoginController(loginProvider: LoginProvider()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor('#3c54b4'),
        title: const Padding(
          padding: EdgeInsets.all(5.0),
          child: Text('List Users'),
        ),
        centerTitle: true,
        actions: [
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          ),
          IconButton(
            icon: const Icon(Icons.logout),
            onPressed: () {
              _loginController.logOut();
            },
          )
        ],
      ),
      body: Obx(() => ListView.builder(
          itemCount: controller.userList.length,
          itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  controller.avatarTextEditingController.text = controller.userList[index].avatar!;
                  controller.emailTextEditingController.text = controller.userList[index].email!;
                  controller.firstNameTextEditingController.text = controller.userList[index].firstName!;
                  controller.lastNameTextEditingController.text = controller.userList[index].lastName!;
                  controller.idTextEditingController.text = controller.userList[index].id.toString();
                  Get.to(DetailsView(
                    avatar: controller.userList[index].avatar,
                    email: controller.userList[index].email,
                    firstName: controller.userList[index].firstName,
                    lastName: controller.userList[index].lastName,
                    id: controller.userList[index].id.toString(),
                  ));
                },
                child: ListTile(
                  dense: true,
                  title: Text(
                    '${controller.userList[index].firstName}',
                    style: const TextStyle(fontSize: 16.0),
                  ),
                  leading: CircleAvatar(
                    backgroundColor: HexColor('#818ec9'),
                    child: Text(
                      controller.userList[index].avatar![0],
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 25.0,
                      ),
                    ),
                  ),
                  subtitle: Text(
                    '${controller.userList[index].lastName}',
                    style: const TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                  trailing: Wrap(
                    spacing: 12, // space between two icons
                    children: const <Widget>[
                      Icon(Icons.star_border), // icon-1
                    ],
                  ),
                ),
              ))),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.to(AddView());
        },
        child: const Icon(Icons.add),
        backgroundColor: HexColor('#3c54b4'),
      ),
    );
  }
}
