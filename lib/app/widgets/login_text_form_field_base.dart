// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

class LoginTextFormFieldBase extends StatelessWidget {
  const LoginTextFormFieldBase(
      {Key? key,
      required this.textEditingController,
      required this.obscureText,
      this.hintText,
      required this.passwordVisibility,
      this.suffix,
      this.textInputFormatter,
      this.returnValidation,
      this.logic,
      this.validator, this.prefix, this.label})
      : super(key: key);

  final TextEditingController textEditingController;
  final bool obscureText;
  final String? hintText;
  final bool passwordVisibility;
  final Widget? suffix;
  final List<TextInputFormatter>? textInputFormatter;
  final dynamic returnValidation;
  final String? logic;
  final String? label;
  final Function? validator;
  final Widget? prefix;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: returnValidation,
      inputFormatters: textInputFormatter,
      controller: textEditingController,
      obscureText: obscureText,
      decoration: InputDecoration(
        labelText: label,
        labelStyle: Get.textTheme.subtitle2!.copyWith(
          fontFamily: 'Lexend Deca',
          color: HexColor('#3c54b4'),
          fontSize: 14,
          fontWeight: FontWeight.normal,
        ),
        hintText: hintText,
        hintStyle: Get.textTheme.subtitle2!.copyWith(
          fontFamily: 'Lexend Deca',
          color: const Color(0xFF95A1AC),
          fontSize: 14,
          fontWeight: FontWeight.normal,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.fromLTRB(20, 24, 20, 24),
        suffixIcon: suffix,
        prefix: prefix
      ),
      style: const TextStyle(
        color: Color(0xFF95A1AC),
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
    );
  }
}
