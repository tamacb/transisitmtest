import 'dart:convert';

import 'package:flutter_app_transisitm/app/data/http_service.dart';
import 'package:flutter_app_transisitm/app/model/action_response.dart';
import 'package:flutter_app_transisitm/app/model/users_model.dart';
import 'package:flutter_app_transisitm/app/utils/utils.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class UsersProvider extends GetConnect {
  Future<UsersModel?> getAllUsers({required String page, required String endUrl}) async {
    Uri _getAllUsers = Uri.parse(HttpService.baseUrl)
        .replace(queryParameters: {"page": page}, pathSegments: ['api',endUrl]);
    logger.wtf(_getAllUsers);
    final response = await http.get(_getAllUsers, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return usersModelFromJson(jsonString);
    }
  }

  Future<ActionResponseModel?> addUser({required String name, required String job}) async {
    Uri _addUser = Uri.parse(HttpService.baseUrl).replace(pathSegments: ['api', 'user']);
    logger.wtf(_addUser);
    var response = await http.post(_addUser,
        headers: HttpService.headers, body: jsonEncode({"name": name, "job": job}));
    logger.wtf(response.statusCode);
    if (response.statusCode == 200 || response.statusCode == 201) {
      return actionResponseModelFromJson(response.body);
    }
  }
}
