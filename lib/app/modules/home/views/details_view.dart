import 'package:flutter/material.dart';
import 'package:flutter_app_transisitm/app/modules/home/controllers/home_controller.dart';
import 'package:flutter_app_transisitm/app/modules/home/views/edit_view.dart';
import 'package:flutter_app_transisitm/app/utils/utils.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

class DetailsView extends GetView<HomeController> {
  final String? email;
  final String? firstName;
  final String? lastName;
  final String? id;
  final String? avatar;

  DetailsView({Key? key, this.email, this.firstName, this.lastName, this.id, this.avatar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor('#3c54b4'),
        title: const Padding(
          padding: EdgeInsets.all(5.0),
          child: Text('Detail Users'),
        ),
        centerTitle: true,
        actions: [
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.delete),
          ),
          GestureDetector(
            onTap: () {
              Get.to(EditView());
            },
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.edit),
            ),
          )
        ],
      ),
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: Container(
                color: HexColor('#3c54b4'),
                child: Image.network(avatar!),
                height: Get.height * 0.4,
              )),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(right: 8.0, left: 8.0),
            child: Card(
              child: ListTile(
                dense: true,
                title: Text(
                  firstName!,
                  style: const TextStyle(fontSize: 16.0),
                ),
                leading: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.call),
                ),
                subtitle: const Text(
                  'first name',
                  style: TextStyle(
                    fontSize: 14.0,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 8.0, left: 8.0),
            child: Card(
              child: ListTile(
                dense: true,
                title: Text(
                  email!,
                  style: const TextStyle(fontSize: 16.0),
                ),
                leading: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.email),
                ),
                subtitle: const Text(
                  'email',
                  style: TextStyle(
                    fontSize: 14.0,
                  ),
                ),
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(right: 8.0, left: 8.0),
            child: Card(
              child: ListTile(
                dense: true,
                title: Text(
                  'instagram',
                  style: TextStyle(fontSize: 16.0),
                ),
                leading: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.share),
                ),
                subtitle: Text(
                  'Social Media',
                  style: TextStyle(
                    fontSize: 14.0,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
