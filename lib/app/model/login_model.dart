import 'dart:convert';

LoginModel loginModelFromJson(String str) => LoginModel.fromJson(json.decode(str));

class LoginModel {
  LoginModel({
    this.id,
    this.token,
    this.error
  });

  final int? id;
  final String? token;
  final String? error;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
    id: json["id"] == null ? null : json["id"],
    token: json["token"] == null ? null : json["token"],
    error: json["error"] == null ? null : json["error"],
  );
}
