import 'package:flutter/material.dart';
import 'package:flutter_app_transisitm/app/modules/home/views/add_view.dart';
import 'package:flutter_app_transisitm/app/modules/login/controllers/login_controller.dart';
import 'package:flutter_app_transisitm/app/modules/login/views/login_view.dart';
import 'package:flutter_app_transisitm/app/widgets/common_text_fileld_base.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class RegisterView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: HexColor('#3c54b4'),
          title: const Padding(
            padding: EdgeInsets.all(5.0),
            child: Text('Transisi Test'),
          ),
          leading: const SizedBox(),
          centerTitle: true),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'Register',
            style: TextStyle(fontSize: 30.0),
          ),
          Card(
            child: TextFormFieldBase(
              textEditingController: controller.emailLoginTextEditingController,
              iconData: Icons.email,
              label: 'email',
              border: InputBorder.none,
            ),
          ),
          Card(
            child: TextFormFieldBase(
              textEditingController: controller.passwordLoginTextEditingController,
              iconData: Icons.lock,
              label: 'password',
              border: InputBorder.none,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RoundedLoadingButton(
                valueColor: Colors.deepPurple,
                color: Colors.white,
                controller: controller.btnController,
                onPressed: () async {
                  await controller.register();
                },
                child: const Text(
                  'Register',
                  style: TextStyle(color: Colors.deepPurple),
                )),
          ),
          GestureDetector(
            onTap: (){
              Get.to(LoginView());
            },
            child: const Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(top: 100.0),
                child: Text(
                  'Punya akun ? coba Login',
                  style: TextStyle(fontSize: 14.0),
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }
}
