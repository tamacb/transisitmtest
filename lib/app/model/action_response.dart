import 'dart:convert';

ActionResponseModel actionResponseModelFromJson(String str) => ActionResponseModel.fromJson(json.decode(str));

class ActionResponseModel {
  ActionResponseModel({
    this.name,
    this.job,
    this.id,
    this.createdAt,
  });

  final String? name;
  final String? job;
  final String? id;
  final DateTime? createdAt;

  factory ActionResponseModel.fromJson(Map<String, dynamic> json) => ActionResponseModel(
    name: json["name"] == null ? null : json["name"],
    job: json["job"] == null ? null : json["job"],
    id: json["id"] == null ? null : json["id"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
  );
}
