import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class TextFormFieldBase extends StatelessWidget {
  final TextEditingController textEditingController;
  final IconData iconData;
  final String label;
  final String? initVal;
  final InputBorder? border;
  const TextFormFieldBase({
    this.border,
    required this.iconData,
    required this.label,
    this.initVal,
    required this.textEditingController,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Icon(
              iconData,
              color: HexColor('#818ec9'),
            ),
          ),
          Expanded(
              child: TextFormField(
                controller: textEditingController,
                initialValue: initVal,
                decoration: InputDecoration(
                    border: border,
                    labelText: label, labelStyle: const TextStyle(color: Colors.black)),
              )),
        ],
      ),
    );
  }
}
