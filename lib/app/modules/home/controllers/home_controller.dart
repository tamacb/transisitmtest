import 'package:flutter/cupertino.dart';
import 'package:flutter_app_transisitm/app/model/user_model.dart';
import 'package:flutter_app_transisitm/app/model/users_model.dart';
import 'package:flutter_app_transisitm/app/modules/home/providers/users_provider.dart';
import 'package:flutter_app_transisitm/app/routes/app_pages.dart';
import 'package:flutter_app_transisitm/app/utils/utils.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  UsersProvider usersProvider;

  HomeController({required this.usersProvider});

  var emailTextEditingController = TextEditingController();
  var avatarTextEditingController = TextEditingController();
  var firstNameTextEditingController = TextEditingController();
  var lastNameTextEditingController = TextEditingController();
  var idTextEditingController = TextEditingController();

  var nameTextEditingController = TextEditingController();
  var jobTextEditingController = TextEditingController();

  final count = 0.obs;
  final page = '1'.obs;

  var userList = <DataUsers>[].obs;

  final isLoadingUsers = false.obs;

  @override
  void onInit() {
    super.onInit();
    getAllUsers(page: page.value);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  Future getAllUsers({String? page}) async {
    try {
      isLoadingUsers.value = true;
      final res = await usersProvider.getAllUsers(
        page: page ?? '1',
        endUrl: 'users',
      );
      userList.assignAll(res!.data!);
    } catch (e) {
      logger.wtf(e);
    } finally {
      isLoadingUsers.value = false;
    }
  }

  Future addUser() async {
    try {
      isLoadingUsers.value = true;
      final res = await usersProvider.addUser(
          name: nameTextEditingController.text, job: jobTextEditingController.text);
      if (res != null) {
        Get.snackbar('this return name ${res.name}', 'this return job ${res.job}');
        await Get.offNamed(Routes.HOME);
      } else {
        Get.snackbar('error', 'add user error !');
      }
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingUsers.value = false;
      nameTextEditingController.clear();
      jobTextEditingController.clear();
    }
  }
}
