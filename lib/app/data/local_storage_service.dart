import 'package:flutter/foundation.dart';
import 'package:flutter_app_transisitm/app/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class LocalStorageService {
  static Future<SharedPreferences> _prefs() async => await SharedPreferences.getInstance();

  static Future<void> setData(String key, {@required dynamic value}) async {
    SharedPreferences sharedPreferences = await _prefs();

    switch (value.runtimeType) {
      case bool:
        await sharedPreferences.setBool(key, value);
        break;
      case double:
        await sharedPreferences.setDouble(key, value);
        break;
      case int:
        await sharedPreferences.setInt(key, value);
        break;
      case String:
        await sharedPreferences.setString(key, value);
        break;
      //* Default is List<String>
      default:
        await sharedPreferences.setStringList(key, value);
        break;
    }

    logger.v('Success Save');
  }

  static Future<dynamic> getData(String key, {required Type type}) async {
    SharedPreferences sharedPreferences = await _prefs();

    dynamic _result;

    switch (type) {
      case bool:
        _result = sharedPreferences.getBool(key);
        break;
      case double:
        _result = sharedPreferences.getDouble(key);
        break;
      case int:
        _result = sharedPreferences.getInt(key);
        break;
      case String:
        _result = sharedPreferences.getString(key);
        break;
      //* Default is List<String>
      default:
        _result = sharedPreferences.getStringList(key);
        break;
    }

    return _result;
  }
}

abstract class LocalStorageKey {
  static const userId = 'user_id';
  static const userIdToken = 'user_id_token';
}
