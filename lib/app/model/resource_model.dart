import 'dart:convert';

ResourceModel resourceModelFromJson(String str) => ResourceModel.fromJson(json.decode(str));

class ResourceModel {
  ResourceModel({
    this.data,
    this.support,
  });

  final Data? data;
  final Support? support;

  factory ResourceModel.fromJson(Map<String, dynamic> json) => ResourceModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    support: json["support"] == null ? null : Support.fromJson(json["support"]),
  );

}

class Data {
  Data({
    this.id,
    this.name,
    this.year,
    this.color,
    this.pantoneValue,
  });

  final int? id;
  final String? name;
  final int? year;
  final String? color;
  final String? pantoneValue;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    year: json["year"] == null ? null : json["year"],
    color: json["color"] == null ? null : json["color"],
    pantoneValue: json["pantone_value"] == null ? null : json["pantone_value"],
  );
}

class Support {
  Support({
    this.url,
    this.text,
  });

  final String? url;
  final String? text;

  factory Support.fromJson(Map<String, dynamic> json) => Support(
    url: json["url"] == null ? null : json["url"],
    text: json["text"] == null ? null : json["text"],
  );
}
