import 'dart:convert';

UsersModel usersModelFromJson(String str) => UsersModel.fromJson(json.decode(str));

class UsersModel {
  UsersModel({
    this.page,
    this.perPage,
    this.total,
    this.totalPages,
    this.data,
    this.support,
  });

  final int? page;
  final int? perPage;
  final int? total;
  final int? totalPages;
  final List<DataUsers>? data;
  final Support? support;

  factory UsersModel.fromJson(Map<String, dynamic> json) => UsersModel(
    page: json["page"] == null ? null : json["page"],
    perPage: json["per_page"] == null ? null : json["per_page"],
    total: json["total"] == null ? null : json["total"],
    totalPages: json["total_pages"] == null ? null : json["total_pages"],
    data: json["data"] == null ? null : List<DataUsers>.from(json["data"].map((x) => DataUsers.fromJson(x))),
    support: json["support"] == null ? null : Support.fromJson(json["support"]),
  );
}

class DataUsers {
  DataUsers({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.avatar,
    this.favorite
  });

  final int? id;
  final String? email;
  final String? firstName;
  final String? lastName;
  final String? avatar;
  final bool? favorite;

  factory DataUsers.fromJson(Map<String, dynamic> json) => DataUsers(
    id: json["id"] == null ? null : json["id"],
    email: json["email"] == null ? null : json["email"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    avatar: json["avatar"] == null ? null : json["avatar"],
  );
}

class Support {
  Support({
    this.url,
    this.text,
  });

  final String? url;
  final String? text;

  factory Support.fromJson(Map<String, dynamic> json) => Support(
    url: json["url"] == null ? null : json["url"],
    text: json["text"] == null ? null : json["text"],
  );
}
