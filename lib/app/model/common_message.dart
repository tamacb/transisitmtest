import 'dart:convert';

CommonMessage commonMessageFromJson(String str) => CommonMessage.fromJson(json.decode(str));

class CommonMessage {
  CommonMessage({
    this.error,
  });

  final String? error;

  factory CommonMessage.fromJson(Map<String, dynamic> json) => CommonMessage(
    error: json["error"] == null ? null : json["error"],
  );
}
