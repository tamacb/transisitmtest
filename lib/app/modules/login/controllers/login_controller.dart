import 'dart:convert';
import 'dart:io';
import 'package:flutter_app_transisitm/app/data/http_service.dart';
import 'package:flutter_app_transisitm/app/model/login_model.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter_app_transisitm/app/data/local_storage_service.dart';
import 'package:flutter_app_transisitm/app/modules/home/controllers/home_controller.dart';
import 'package:flutter_app_transisitm/app/modules/home/providers/users_provider.dart';
import 'package:flutter_app_transisitm/app/modules/login/providers/login_provider.dart';
import 'package:flutter_app_transisitm/app/routes/app_pages.dart';
import 'package:flutter_app_transisitm/app/utils/utils.dart';
import 'package:get/get.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class LoginController extends GetxController {
  final emailLoginTextEditingController = TextEditingController();
  final passwordLoginTextEditingController = TextEditingController();

  LoginProvider loginProvider;

  LoginController({required this.loginProvider});

  final count = 0.obs;

  final HomeController _homeController = Get.put(HomeController(usersProvider: UsersProvider()));
  final RoundedLoadingButtonController btnController = RoundedLoadingButtonController();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  Future register() async {
    try {
      final res = await loginProvider.login(
          email: emailLoginTextEditingController.text, password: emailLoginTextEditingController.text, endUrl: 'login');
      if (res!.token!.isNotEmpty) {
        await LocalStorageService.setData(LocalStorageKey.userIdToken, value: res.token);
        await Get.offNamed(Routes.HOME);
      } else {
        Get.snackbar('${res.error}', 'login error !');
      }
    } catch (e) {
      logger.e(e);
    } finally {
      btnController.reset();
    }
  }

  Future login() async {
    try {
      final res = await loginProvider.login(
          email: emailLoginTextEditingController.text, password: emailLoginTextEditingController.text, endUrl: 'login');
      if (res!.token!.isNotEmpty) {
        await LocalStorageService.setData(LocalStorageKey.userIdToken, value: res.token);
        await Get.offNamed(Routes.HOME);
      } else {
        Get.snackbar('${res.error}', 'login error !');
      }
    } catch (e) {
      logger.e(e);
    } finally {
      btnController.reset();
    }
  }

  Future logOut() async {
    final _userIdToken = await LocalStorageService.getData(LocalStorageKey.userIdToken, type: String);
    if (_userIdToken != "") {
      await LocalStorageService.setData(LocalStorageKey.userIdToken, value: "");
      await Get.offNamedUntil(Routes.LOGIN, ModalRoute.withName(Routes.LOGIN));
    } else {
      Get.snackbar('failed', 'Log Out Failed');
    }
  }
}
