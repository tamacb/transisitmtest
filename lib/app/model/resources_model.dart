import 'dart:convert';

ResourcesModel resourcesModelFromJson(String str) => ResourcesModel.fromJson(json.decode(str));

class ResourcesModel {
  ResourcesModel({
    this.page,
    this.perPage,
    this.total,
    this.totalPages,
    this.data,
    this.support,
  });

  final int? page;
  final int? perPage;
  final int? total;
  final int? totalPages;
  final List<DataResources>? data;
  final Support? support;

  factory ResourcesModel.fromJson(Map<String, dynamic> json) => ResourcesModel(
    page: json["page"] == null ? null : json["page"],
    perPage: json["per_page"] == null ? null : json["per_page"],
    total: json["total"] == null ? null : json["total"],
    totalPages: json["total_pages"] == null ? null : json["total_pages"],
    data: json["data"] == null ? null : List<DataResources>.from(json["data"].map((x) => DataResources.fromJson(x))),
    support: json["support"] == null ? null : Support.fromJson(json["support"]),
  );
}

class DataResources {
  DataResources({
    this.id,
    this.name,
    this.year,
    this.color,
    this.pantoneValue,
  });

  final int? id;
  final String? name;
  final int? year;
  final String? color;
  final String? pantoneValue;

  factory DataResources.fromJson(Map<String, dynamic> json) => DataResources(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    year: json["year"] == null ? null : json["year"],
    color: json["color"] == null ? null : json["color"],
    pantoneValue: json["pantone_value"] == null ? null : json["pantone_value"],
  );
}

class Support {
  Support({
    this.url,
    this.text,
  });

  final String? url;
  final String? text;

  factory Support.fromJson(Map<String, dynamic> json) => Support(
    url: json["url"] == null ? null : json["url"],
    text: json["text"] == null ? null : json["text"],
  );
}
