import 'dart:convert';
import 'dart:io';

class HttpService {
  static const baseUrl =
      "https://reqres.in/";

  static const Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: 'application/json',
    HttpHeaders.acceptHeader: 'application/json',
  };

  Map<String, String> authReqHeader({String? bearer}){
    return {
      HttpHeaders.acceptHeader: 'application/json',
      HttpHeaders.authorizationHeader: '$bearer',
    };
  }
}
