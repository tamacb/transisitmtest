import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_transisitm/app/modules/home/controllers/home_controller.dart';
import 'package:flutter_app_transisitm/app/widgets/common_text_fileld_base.dart';
import 'package:flutter_app_transisitm/app/widgets/login_text_form_field_base.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

class AddView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(
            backgroundColor: HexColor('#3c54b4'),
            title: const Padding(
              padding: EdgeInsets.all(5.0),
              child: Text('Add Users'),
            ),
            centerTitle: true,
            leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Icons.close),
            ),
            actions: [
              GestureDetector(
                onTap: () {
                  controller.addUser();
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: (controller.isLoadingUsers.value) ? const Icon(Icons.save) : const Icon(Icons.save),
                ),
              )
            ],
          ),
          body: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                    child: CircleAvatar(
                  backgroundColor: HexColor('#3c54b4'),
                  maxRadius: 80.0,
                  child: const SizedBox(
                    height: 150.0,
                    width: 150.0,
                    child: Icon(
                      Icons.photo_camera,
                      color: Colors.white,
                    ),
                  ),
                )),
              ),
              TextFormFieldBase(
                textEditingController: controller.nameTextEditingController,
                iconData: Icons.account_circle,
                label: 'name',
              ),
              TextFormFieldBase(
                textEditingController: controller.jobTextEditingController,
                iconData: Icons.work,
                label: 'job',
              ),
            ],
          ),
        ));
  }
}
