import 'package:flutter_app_transisitm/app/data/local_storage_service.dart';
import 'package:flutter_app_transisitm/app/routes/app_pages.dart';
import 'package:flutter_app_transisitm/app/utils/utils.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  //TODO: Implement SplashController

  final count = 0.obs;
  @override
  void onInit() async {
    super.onInit();
    await checkLoginState();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  Future checkLoginState() async {
    final _idToken = await LocalStorageService.getData(LocalStorageKey.userIdToken, type: String);
    logger.wtf(_idToken);
    if (_idToken == null) await LocalStorageService.setData(LocalStorageKey.userIdToken, value: "");

    await Future.delayed(2.seconds);

    (_idToken == "" || _idToken == null) ? Get.offNamed(Routes.LOGIN) : Get.offNamed(Routes.HOME);

    logger.wtf('ini token $_idToken');
  }
}
