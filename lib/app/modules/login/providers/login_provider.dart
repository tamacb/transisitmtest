import 'dart:convert';
import 'dart:io';

import 'package:flutter_app_transisitm/app/data/http_service.dart';
import 'package:flutter_app_transisitm/app/model/login_model.dart';
import 'package:flutter_app_transisitm/app/utils/utils.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class LoginProvider extends GetConnect {
  // Future<LoginModel?> register({required String email, required String password}) async {
  //   Uri _register = Uri.parse(HttpService.baseUrl).replace(pathSegments: ['api', 'register']);
  //   final register = await http.post(
  //     _register,
  //     body: jsonEncode({
  //       "email": email,
  //       "password": password,
  //     }),
  //     headers: {
  //       HttpHeaders.contentTypeHeader: 'application/json',
  //     },
  //   );
  //   logger.wtf(register.body);
  //   return loginModelFromJson(register.body);
  // }

  Future<LoginModel?> register({required String email, required String password}) async {
    Uri _register = Uri.parse(HttpService.baseUrl).replace(pathSegments: ['api', 'register']);
    logger.wtf(_register);
    var response = await http.post(_register,
        headers: HttpService.headers, body: jsonEncode({"email": email, "password": password}));
    logger.wtf(response.body);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return loginModelFromJson(jsonString);
    }
  }

  Future<LoginModel?> login({required String endUrl, required String email, required String password}) async {
    Uri _login = Uri.parse(HttpService.baseUrl).replace(pathSegments: ['api', endUrl]);
    logger.wtf(_login);
    var response = await http.post(_login,
        headers: HttpService.headers, body: jsonEncode({"email": email, "password": password}));
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return loginModelFromJson(jsonString);
    }
  }
}
