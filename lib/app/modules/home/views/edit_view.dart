import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_transisitm/app/modules/home/controllers/home_controller.dart';
import 'package:flutter_app_transisitm/app/modules/home/views/add_view.dart';
import 'package:flutter_app_transisitm/app/widgets/common_text_fileld_base.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

class EditView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor('#3c54b4'),
        title: const Padding(
          padding: EdgeInsets.all(5.0),
          child: Text('Edit User'),
        ),
        centerTitle: true,
        actions: const [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.save),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    child: Container(
                      color: HexColor('#3c54b4'),
                      child: Image.network(controller.avatarTextEditingController.text),
                      height: Get.height * 0.4,
                    )),
              ],
            ),
            TextFormFieldBase(
              textEditingController: controller.emailTextEditingController,
              iconData: Icons.email,
              label: 'email',
            ),
            TextFormFieldBase(
              textEditingController: controller.firstNameTextEditingController,
              iconData: Icons.account_circle,
              label: 'first name',
            ),
            TextFormFieldBase(
              textEditingController: controller.lastNameTextEditingController,
              iconData: Icons.account_circle_outlined,
              label: 'last name',
            ),
          ],
        ),
      ),
    );
  }
}
